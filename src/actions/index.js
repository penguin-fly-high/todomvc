import { ADD_TODO, FILTER, CLEAR_FILTER, MARK_COMPLETED } from '../utils/constants';

export const clearCompletedItems = () => {
  return {
    type: CLEAR_FILTER,
  }
}

export const addToDo = todo => {
  return {
    type: ADD_TODO,
    payload: todo
  };
}

export const filter = criteria => {
  return {
    type: FILTER,
    payload: criteria
  }
}

export const markCompleted = id => {
  return {
    type: MARK_COMPLETED,
    payload: id
  }
}

