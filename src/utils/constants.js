export const ADD_TODO = 'ADD_TODO';
export const FILTER = 'FILTER_ITEMS';
export const SHOW_ALL = 'ALL_ITEMS';
export const SHOW_COMPLETED = 'COMPLETED_ITEMS';
export const SHOW_ACTIVE = 'ACTIVE_ITEMS';
export const CLEAR_FILTER = 'CLEAR_FILTER';
export const MARK_COMPLETED = 'MARK_COMPLETED';