import React, { Component } from 'react'
import TodoList from '../containers/TodoList';
import Filters from '../containers/Filters';
import AddToDo from '../containers/AddToDo';

 class App extends Component {
  render() {
    return (
      <div>
        <AddToDo />
        <TodoList />
        <Filters />
      </div>
    )
  }
}

export default App;
