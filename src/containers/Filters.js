import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { filter } from "..//actions/index";
import { clearCompletedItems } from '../actions/index';
import {
  SHOW_ALL,
  SHOW_ACTIVE,
  SHOW_COMPLETED,
  CLEAR_FILTER
} from "../utils/constants";

class Filters extends Component {
  constructor(props) {
    super(props);
  }

  onFilterClick = (e) => {
    this.props.filter(e.target.value);
  }

  onClearCompletedClick = (e) => {
    this.props.clearCompletedItems();
  }

  render() {
    return (
      <div className="container" >
        <button onClick={this.onFilterClick} value={SHOW_ALL}>All</button>
        <button onClick={this.onFilterClick} value={SHOW_ACTIVE}>Active</button>
        <button onClick={this.onFilterClick} value={SHOW_COMPLETED}>Completed</button>
        <button onClick={this.onClearCompletedClick} value={CLEAR_FILTER}>Clear Completed</button>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ filter: filter, clearCompletedItems: clearCompletedItems }, dispatch);
}

function mapStateToProps(state) {
  return { filter: state.filter };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Filters);
