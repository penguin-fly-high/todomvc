import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { clearCompletedItems } from "../actions/index";
import { CLEAR_FILTER, SHOW_ACTIVE, SHOW_ALL, SHOW_COMPLETED } from "../utils/constants";
import Todo from "./Todo";

const TodoList = props => {
  const { filter } = props;
  if (filter === CLEAR_FILTER) {
    props.clearCompletedItems();
  }
  return (
    <div className="container">
      <ul>
        {props.availableTodo.map(todo => {
          return (<Todo key={todo.id} value={todo} />)
        })}
      </ul>
    </div>
  );
}

const filterTodoList = (todos, criteria) => {
  if (criteria === SHOW_ALL) {
    return todos;
  } else if (criteria === SHOW_ACTIVE) {
    return _.filter(todos, { isActive: true });
  } else if (criteria === SHOW_COMPLETED) {
    return _.filter(todos, { isCompleted: true });
  } 
};

const mapStateToProps = state => {
  return {
    availableTodo: !state.filter ? state.todoList : filterTodoList(state.todoList, state.filter),
    filter: state.filter
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { clearCompletedItems: clearCompletedItems },
    dispatch
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);
