import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { addToDo } from "../actions/index";
import uuidv1 from "uuid/v1";

class AddToDo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: ""
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    this.props.addToDo({
      id: uuidv1(),
      content: this.state.searchTerm,
      isActive: true,
      isCompleted: false
    });
    this.setState({
      searchTerm: ""
    });
  }

  onInputChange = (e) => {
    this.setState({
      searchTerm: e.target.value
    });
  }

  render() {
    return (
      <div className="container">
        <form onSubmit={this.onFormSubmit}>
          <div className="form-group">
            <input
              className="form-control"
              placeholder="What needs to be done?"
              type="text"
              value={this.state.searchTerm}
              onChange={this.onInputChange}
            />
          </div>
        </form>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ addToDo: addToDo }, dispatch);
}

export default connect(
  null,
  mapDispatchToProps
)(AddToDo);
