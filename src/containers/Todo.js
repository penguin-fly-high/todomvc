import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { markCompleted } from "../actions/index";

const Todo = props => {
  const { value } = props;
  const onItemClicked = e => {
    props.markCompleted(props.value.id);
  };

  return (
    <li
      onClick={onItemClicked}
      style={{
        textDecoration:
          props.value.isCompleted === false ? "" : "line-through"
      }}
    >
      {value.content}
    </li>
  );
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ markCompleted: markCompleted }, dispatch);
}

export default connect(
  null,
  mapDispatchToProps
)(Todo);
