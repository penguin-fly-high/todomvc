import { FILTER, SHOW_ALL } from "../utils/constants";

export default (state = SHOW_ALL, action) => {
  switch(action.type) {
    case FILTER:
      return action.payload;
    default: 
      return state;
  }
}