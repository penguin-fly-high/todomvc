import { combineReducers } from 'redux';
import FilterReducer from './FilterReducer';
import TodoListReducer from './TodoListReducer';

export default combineReducers({
  todoList: TodoListReducer,
  filter: FilterReducer,
});