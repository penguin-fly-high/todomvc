import _ from "lodash";
import { ADD_TODO, CLEAR_FILTER, MARK_COMPLETED } from "../utils/constants";

const initialState = [
  { id: "1", content: "Rabbit", isActive: true, isCompleted: true },
  { id: "2", content: "Tomato", isActive: false, isCompleted: false },
  { id: "3", content: "OK", isActive: true, isCompleted: false },
  { id: "4", content: "Hole", isActive: true, isCompleted: false }
];

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return [...state, action.payload];
    case CLEAR_FILTER:
      return _.filter(state, { isCompleted: false });
    case MARK_COMPLETED:
    console.log(state);
      return state.map(todo => todo.id === action.payload ? {...todo, isCompleted: !todo.isCompleted} : todo);
  }

  return state;
}
